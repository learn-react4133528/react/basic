export const Props = (props) => {
    return (
        <>
            <p>Hello I`m {props.name}. I years old {props.age}.</p>
            <>{props.children}</>
        </>
    )
}