import {Component} from "react";
import {ItemList} from "./ItemList";

export class RenderList extends Component {
    state = {
        persons: [
            {
                id: 1,
                name: 'John',
                age: 23
            },
            {
                id: 2,
                name: 'Doe',
                age: 54
            },
            {
                id: 3,
                name: 'Peter',
                age: 101
            }
        ]
    }

    render() {
        const personList = this.state.persons.map((person) => <ItemList key={person.id} name={person.name} age={person.age} />)

        return (
            <div>
                {personList}
            </div>
        )
    }
}