import {memo} from "react";

export const MemoComp = memo(({name}) => {
    console.log('********Rendering Memo Component********')
    return (
        <div>
            {name}
        </div>
    )
})