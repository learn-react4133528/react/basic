import './StyleSheet.css';
export const StyleSheet = (props) => {
    const primary  = props.primary ? 'primary' : 'secondary';
    return <p className={`${primary} font-size`}>Style sheet</p>
}