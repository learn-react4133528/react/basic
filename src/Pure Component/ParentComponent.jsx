import React, {Component} from 'react';
import RegComponent from "./RegComponent";
import {PureComp} from "./PureComponent";
import {MemoComp} from "../Memo/MemoComp";

export class ParentPureComponent extends Component {
    constructor(props) {
        super(props);
        this.state = {
            name: 'Sasha'
        }
    }
    componentDidMount() {
        // setInterval(() => {
        //     this.setState({
        //         name: 'Sasha'
        //     })
        // }, 2000)
    }

    render() {
        console.log('*************Parent render*************')
        return (
            <div>
                ParentComponent
                <PureComp name={this.state.name} />
                <RegComponent name={this.state.name} />
                <MemoComp name={this.state.name} />
            </div>
        );
    }
}
