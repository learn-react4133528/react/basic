import {Component} from "react";
import {ChildrenLiftingUp} from "./ChildrenLiftingUp";

export class LiftingUp extends Component {
    state = {
        test: false
    }

    changeTest = () => {
        this.setState(prevState => ({
            test: !prevState.test
        }))
    }

    render() {
        // console.log(this.state)
        return (
            <ChildrenLiftingUp changeParentState={this.changeTest}/>
        )
    }
}