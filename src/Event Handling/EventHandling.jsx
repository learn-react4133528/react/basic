import {Component} from "react";

export class EventHandling extends Component {
    constructor(props) {
        super(props);

        this.bindingEvent = this.bindingEvent.bind(this)
    }
    callbackEvent() { //low performance
        console.log('Callback event')
    }
    bindingEvent() {
        console.log('Binding event')
    }
    classFieldEvent = () => {
        console.log('Class field event')
    }

    render() {
        return (
            <div>
                <p onClick={() => this.callbackEvent()}>Callback event</p>
                <p onClick={this.bindingEvent}>Binding event</p>
                <p onClick={this.classFieldEvent}>Class public field event</p>
            </div>
        );
    }
}

export default EventHandling;