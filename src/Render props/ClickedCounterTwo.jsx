import {Component} from "react";

class ClickedCounterTwo extends Component {
    render() {
        const { count, incrementCount } = this.props;
        return (
            <button onClick={incrementCount}>Clicked {count} times</button>
        )
    }
}

export default ClickedCounterTwo;