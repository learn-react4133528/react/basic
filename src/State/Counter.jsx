import {Component} from "react";

export class Counter extends Component {
    constructor(props) {
        super(props);

        this.state = {
            count: 0
        }
    }

    increment = () => { // asynchronously
        this.setState({
            count: this.state.count + 1
        })
    }

    incrementThree = () => {
        this.increment()
        this.increment()
        this.increment()
    }

    decrement = () => { // synchronously
        this.setState((prevState) => ({
            count: prevState.count - 1
        }))
    }

    decrementThree = () => {
        this.decrement()
        this.decrement()
        this.decrement()
    }

    clear = () => {
        this.setState({
            count: 0
        })
    }

    render() {
        return (
            <>
                <p>{this.state.count}</p>
                <button onClick={this.decrementThree}>-3</button>
                <button onClick={this.decrement}>-</button>
                <button onClick={this.clear}>Clear</button>
                <button onClick={this.increment}>+</button>
                <button onClick={this.incrementThree}>+3</button>
            </>
        )
    }
}