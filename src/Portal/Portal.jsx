import {createPortal} from "react-dom";

export const Portal = () => {
    return createPortal(
        <h1>Portal</h1>,
        document.getElementById('portal-root')
    )
}