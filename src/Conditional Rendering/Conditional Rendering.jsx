import {Component} from "react";

export class ConditionalRendering extends Component {
   state = {
        test: false
   }

   render() {
       return (
           <p>{this.state.test ? 'Значение true' : 'Значение false'}</p>
       )
   }
}