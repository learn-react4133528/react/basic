import {FRInput} from "./FRInput";
import {Component, createRef} from "react";

export class FRParentInput extends Component {
    constructor(props) {
        super(props);
        this.inputRef = createRef();
    }

    clickHandler = () => {
        this.inputRef.current.focus();
    }
    render() {
        return (
            <div>
                <FRInput ref={this.inputRef}/>
                <button onClick={this.clickHandler}>Focus input</button>
            </div>
        )
    }
}