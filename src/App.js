import './App.css';
import {LessonWrapper} from "./layout/LessonWrapper";

import {HelloWorld} from "./Hello world/HelloWorld";
import {ClassComponent} from "./Components/ClassComponent";
import {FunctionComponent} from "./Components/FunctionComponent";
import {Props} from "./Props/Props";
import {Counter} from "./State/Counter";
import EventHandling from "./Event Handling/EventHandling";
import {LiftingUp} from "./Lifting Up/LiftingUp";
import {ConditionalRendering} from "./Conditional Rendering/Conditional Rendering";
import {RenderList} from "./Render List/RenderList";
import {StyleSheet} from "./CSS/StyleSheet";
import {ModuleCSS} from "./CSS/ModuleCSS";
import {Inline} from "./CSS/Inline";
import {ControlledComponents} from "./Controlled Components/ControlledComponents";
import LifecycleA from "./Lifecycle/LifecycleA";
import Http from "./HTTP/Http";
import {ParentPureComponent} from "./Pure Component/ParentComponent";
import {Refs} from "./Refs/Refs";
import {FRParentInput} from "./Forwarding Refs/FRParentInput";
import {ExampleError} from "./Error Boundary/ExampleError";
import {ErrorBoundary} from "./Error Boundary/ErrorBoundary";
import ClickCounter from "./HOC/ClickCounter";
import HoverCounter from "./HOC/HoverCounter";
import ClickedCounterTwo from "./Render props/ClickedCounterTwo";
import HoverCounterTwo from "./Render props/HoverCounterTwo";
import CounterRenderProps from "./Render props/CounterRenderProps";
import {ComponentC} from "./Context/ComponentC";
import {UserProvider} from "./Context/userContext";
// import User from "./Render props/User";
// import {Portal} from "./Portal/Portal";

function App() {
    return (
        <div className="App">
            <LessonWrapper lesson={1}>
                <HelloWorld />
                <ClassComponent />
                <FunctionComponent />
                <Props name={'Sasha'} age={25}/>
                <Props name={'Anton'} age={20}/>
                <Props name={'Nikita'} age={21}>
                    <p>I`m frontend developer.</p>
                </Props>
            </LessonWrapper>
            <LessonWrapper lesson={2}>
                <Counter />
                <EventHandling />
                <LiftingUp />
                <ConditionalRendering />
                <RenderList />
            </LessonWrapper>
            <LessonWrapper lesson={3}>
                <StyleSheet primary/>
                <ModuleCSS />
                <Inline />
                <ControlledComponents />
            </LessonWrapper>
            <LessonWrapper lesson={4}>
                <LifecycleA />
                <Http />
            </LessonWrapper>
            <LessonWrapper lesson={5}>
                <ParentPureComponent />
                <Refs />
                <FRParentInput />
                {/*<Portal />*/}
            </LessonWrapper>
            <LessonWrapper lesson={6}>
                <ErrorBoundary>
                    <ExampleError text={'lesson'}/>
                    <ExampleError text={'super'}/>
                    {/*<ExampleError text={'error'}/>*/}
                </ErrorBoundary>
                {/*HOC*/}
                <ClickCounter name={'Sasha'} />
                <HoverCounter />
                {/*Render Props*/}
                <ClickedCounterTwo />
                <HoverCounterTwo />
                <CounterRenderProps render = {(count, incrementCount) => <ClickedCounterTwo count={count} incrementCount={incrementCount} />} />
                <CounterRenderProps render = {(count, incrementCount) => <HoverCounterTwo count={count} incrementCount={incrementCount} />} />
                {/*<User render={(isLoggedIn) => isLoggedIn ? 'Sasha' : 'Guest'} />*/}
                <UserProvider value={'Sasha'}>
                    <ComponentC />
                </UserProvider>
            </LessonWrapper>
        </div>
    );
}

export default App;
