import {createContext} from "react";

const UserContext = createContext('basic');

const UserProvider = UserContext.Provider;
const UserConsumer = UserContext.Consumer;

export {UserProvider, UserConsumer};
export default UserContext;