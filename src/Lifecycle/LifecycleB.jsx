import React, {Component} from 'react';

class LifecycleB extends Component {
    constructor(props) {
        super(props);

        this.state = {
            name: 'Sasha'
        }
        console.log('LifecycleB constructor')
    }

    static getDerivedFromProps(props, state) {
        console.log('LifecycleB getDerivedFromProps')
        return null
    }

    componentDidMount() {
        console.log('LifecycleB componentDidMount')
    }

    shouldComponentUpdate(nextProps, nextState) {
        console.log('LifecycleB shouldComponentUpdate')
        return true
    }

    getSnapshotBeforeUpdate(prevProps, prevState) {
        console.log('LifecycleB getSnapshotBeforeUpdate')
        return null
    }

    componentDidUpdate(prevProps, prevState) {
        console.log('LifecycleB componentDidUpdate')
    }

    render() {
        console.log('LifecycleB render')
        return (
            <div>
                LifecycleB
            </div>
        );
    }
}

export default LifecycleB;