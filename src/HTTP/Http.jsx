import React, {Component} from 'react';
import axios from "axios";

class Http extends Component {
    constructor(props) {
        super(props);

        this.state = {
            posts: [],
            errorMsg: ""
        }
    }
    componentDidMount() {
        // fetch('https://jsonplaceholder.typicode.com/posts')
        //     .then(res => res.json())
        //     .then(json => {
        //         this.setState({
        //             posts: json
        //         })
        //     })
        //     .catch(err => {
        //         console.log(err)
        //     })
        axios.get('https://jsonplaceholder.typicode.com/posts/1')
            .then(res => {
                this.setState({
                    posts: [res.data]
                })
            })
            .catch(err => {
                console.log(err)
                this.setState({
                    errorMsg: "Error"
                })
            })
        // fetch('https://jsonplaceholder.typicode.com/posts', {
        //     method: 'POST',
        //     body: JSON.stringify({
        //         title: 'foo',
        //         body: 'bar',
        //         userId: 1,
        //     }),
        //     headers: {
        //         'Content-type': 'application/json; charset=UTF-8',
        //     },
        // })
        //     .then((response) => response.json())
        //     .then((json) => console.log(json));
        // axios.post('https://jsonplaceholder.typicode.com/posts', {
        //     title: 'foo',
        //     body: 'bar'
        // })
        //     .then(res => {
        //         this.setState({
        //             posts:[res.data]
        //         })
        //     })
        //     .catch(err => {
        //         console.log(err)
        //         this.setState({
        //             errorMsg: "Error"
        //         })
        //     })
    }

    render() {
        return (
            <div>
                {   !!this.state.posts.length &&
                    this.state.posts.map((post) => <p key={post.id}>{post.title}</p>)
                }
                {this.state.errorMsg && <div>{this.state.errorMsg}</div>}
            </div>
        );
    }
}

export default Http;